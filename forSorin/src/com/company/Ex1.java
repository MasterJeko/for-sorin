package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Ex1 {

    public static void arrayEx() {
        Scanner scanner = new Scanner(System.in);

        Integer [] array = new Integer[3];

        for (int i = 0; i < array.length; i++) {
            System.out.println("Enter a number: ");
            array[i] = scanner.nextInt();
        }

        System.out.println("Total numbers are: " + array.length);
        System.out.println("The first index number is: " + Arrays.asList(array).indexOf(1));
        System.out.println("The last index number is: " + Arrays.asList(array).indexOf(3));
    }
}
