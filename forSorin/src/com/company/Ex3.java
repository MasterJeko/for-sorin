package com.company;

import java.util.Scanner;

public class Ex3 {

    public static void evenOrOdd() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a number: ");
        int a = scanner.nextInt();
        if (a%2==0) {
            System.out.println("even number");
        } else if (a%2==1) {
            System.out.println("odd number");
        }
    }
}
