package com.company;

import java.util.Scanner;

public class Ex2 {

    public static void ageEx() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input age: ");
        int age = scanner.nextInt();
        if ( age < 12 && age > 0) {
            System.out.println("I'm a child");
        } else if (age >= 12 && age <= 18) {
            System.out.println("I'm a teenager");
        } else if (age > 18 &&  age < 65){
            System.out.println("I'm a grown up");
        } else if (age >= 65 &&  age <= 135){
            System.out.println("I'm old");
        } else System.out.println("Invalid age");
    }
}
