package com.company;

import java.util.Scanner;

public class Ex5 {

    public static void colors() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter G, Y or V only: ");
        String color = scanner.next().toLowerCase().toUpperCase();
        switch (color) {
            case ("G"):
                System.out.println("Green");
                break;
            case ("Y"):
                System.out.println("Yellow");
                break;
            case ("R"):
                System.out.println("Red");
                break;
            default:
                System.out.println("Enter only the letters from above");
        }
    }
}
